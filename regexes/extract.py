# week 2 problem for coursera class "using python to access network data"

# parse a file and sum all the numbers found in the file

import re

def sumNums(nums):
    total = 0
    for num in nums:
        total += int(num)

    return total


fh = open("/Users/phollenb/git/coursera-python/regexes/regex_sum_315817.txt")
totalSum = 0

for line in fh:
    nums = re.findall("\d+", line)
    totalSum += sumNums(nums)

print(totalSum)
